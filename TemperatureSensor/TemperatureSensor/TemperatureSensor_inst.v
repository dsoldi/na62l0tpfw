	TemperatureSensor u0 (
		.ce         (<connected-to-ce>),         //         ce.ce
		.clk        (<connected-to-clk>),        //        clk.clk
		.clr        (<connected-to-clr>),        //        clr.reset
		.tsdcaldone (<connected-to-tsdcaldone>), // tsdcaldone.tsdcaldone
		.tsdcalo    (<connected-to-tsdcalo>)     //    tsdcalo.tsdcalo
	);

